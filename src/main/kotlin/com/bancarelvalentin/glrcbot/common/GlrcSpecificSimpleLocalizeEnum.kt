package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.i18n.I18nDictionary

enum class GlrcSpecificSimpleLocalizeEnum : I18nDictionary {
    PROCESS__ON_BASTARD__RESPONSE,
    PROCESS__ON_WELCOME__RESPONSE
    ;
    
    override val key: String
        get() {
            val identity = (ConfigHandler.config as CommonConfig).identity.name.toLowerCase()
            val key = I18nDictionary.enumToKey(this)
            return "$identity.$key"
        }
}
