package com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import net.dv8tion.jda.api.events.role.update.RoleUpdateColorEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateNameEvent


class GameGrantsOnRoleUpdate(process: Process) : EventListener(process) {
    override fun onRoleUpdateColor(event: RoleUpdateColorEvent) {
        GameGrantsHelper.roleTrigger(event.role)
    }
    
    override fun onRoleUpdateName(event: RoleUpdateNameEvent) {
        GameGrantsHelper.roleTrigger(event.role)
    }
}
