package com.bancarelvalentin.glrcbot.common.process.common.command.gamesupport

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import java.util.function.BiConsumer

class GameSupportAddTextCommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_SUPPORT_ADD__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_SUPPORT_ADD__DESC)
    override val sample = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_SUPPORT_ADD__SAMPLE)
    
    override val whitelistRoleIds = arrayOf(HardCodedValues.ROLE__MAITRE_EQUIPAGE)
    override val patterns = arrayOf("game_support_add", "gsa")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> =
        arrayOf(GameNameParam::class.java)
    
    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        
        val gameName = request.getParamString(0)!!
        
        val textChannel = request.guild!!.createTextChannel(
            gameName,
            request.guild!!.getCategoryById(HardCodedValues.CATEGORY_GAMES_TEXT_CHATS)
        ).complete()
        val patchChannel = request.guild!!.createTextChannel(
            "$gameName-patch",
            request.guild!!.getCategoryById(HardCodedValues.CATEGORY_GAMES_PATCH_CHATS)
        ).complete()
        //val voiceChannel = request.guild!!.createVoiceChannel(
        //    channelsName,
        //    request.guild!!.getCategoryById(HardCodedValues.CATEGORY_GAMES_VOICE_CHATS)
        //).complete()
        
        val role = request.guild!!.createRole()
            .setColor((ConfigHandler.config as CommonConfig).playerRoleColor)
            .setName("${gameName.capitalize()} ${HardCodedValues.playerRoleSuffix}")
            .setMentionable(true)
            .complete()
        
        textChannel
            .createPermissionOverride(role)
            .grant((ConfigHandler.config as CommonConfig).viewPermissionBit)
            .complete()
        patchChannel
            .createPermissionOverride(role)
            .grant((ConfigHandler.config as CommonConfig).viewPermissionBit)
            .complete()
        //      .complete()
        
        val roleAssignChannel = gateway.getTextChannelById(HardCodedValues.CHANNEL__ROLE_ASSIGNEMENT)
        gateway.getTextChannelById(HardCodedValues.CHANNEL__ANNOUCEMENTS)
            ?.sendMessage(
                CommonUtils.localize(
                    GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_SUPPORT_ADD__ANOUNCEMENT,
                    // voiceChannel?.asMention ?: "",
                    textChannel?.asMention ?: "",
                    patchChannel?.asMention ?: "",
                    role.asMention,
                    roleAssignChannel?.asMention ?: ""
                )
            )
            ?.queue()
        patchChannel.sendMessage(CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_SUPPORT_ADD__PATCHBOT_REMINDER, FormatingUtils.formatRoleId(HardCodedValues.ROLE__CO_CAPITAINE)))
            .queue()
        
    }
}
