package com.bancarelvalentin.glrcbot.common.process.common.background.onbastardmsgbot


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum


class OnBastardMsgBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__ON_BASTARD_MSG__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__ON_BASTARD_MSG__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(OnBastardMsgOnMessage(this))
    }
}