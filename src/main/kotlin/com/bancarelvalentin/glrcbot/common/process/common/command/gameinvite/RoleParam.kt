package com.bancarelvalentin.glrcbot.common.process.common.command.gameinvite

import com.bancarelvalentin.ezbot.process.command.param.RoleCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class RoleParam : RoleCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__GAME_INVITE__ROLE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__GAME_INVITE__ROLE__DESC)
}