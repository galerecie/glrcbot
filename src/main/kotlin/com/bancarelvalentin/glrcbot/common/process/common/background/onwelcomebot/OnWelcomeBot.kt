package com.bancarelvalentin.glrcbot.common.process.common.background.onwelcomebot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum


class OnWelcomeBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__ON_WELCOME__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__ON_WELCOME__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(OnWelcomeBotOnJoin(this))
    }
}