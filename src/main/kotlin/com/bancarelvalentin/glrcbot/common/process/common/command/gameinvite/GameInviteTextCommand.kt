package com.bancarelvalentin.glrcbot.common.process.common.command.gameinvite

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.Constants
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import com.bancarelvalentin.glrcbot.common.process.common.background.deletetempchannelondisconnectbot.DeleteTempChannelOndisconnectBotOnDisconnect
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.VoiceChannel
import java.util.function.BiConsumer

class GameInviteTextCommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_INVITE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_INVITE__DESC)
    override val sample = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_INVITE__SAMPLE)
    
    override val patterns = arrayOf("invite", "i")
    
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> =
        arrayOf(RoleParam::class.java, MessageParam::class.java)
    
    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        val role = request.getParamRole(0)!!
        val msg = request.joinParams(1)
        val sentMessage = trigger(request.guild!!, request.guild!!.retrieveMember(request.user).complete(), role, msg)
        
        response.message = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_INVITE__SUCCESS, FormatingUtils.messageToLink(sentMessage))
    }
    
    private fun getEmbed(guild: Guild, author: Member, role: Role, msg: String?): MessageEmbed {
        val builder = EmbedBuilder()
            .setDescription("**${CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_INVITE__EMBED_DESC, author.asMention)}**\n${msg ?: ""}")
            .setFooter(CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_INVITE__EMBED_FOOTER))
        
        builder.setAuthor("${EmojiEnum.MICROPHONE.tounicode()} ${CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_INVITE__EMBED_JOIN_VOCAL)}", createTemporaryChannel(guild, role).createInvite().complete().url)
        return builder.build()
    }
    
    fun createTemporaryChannel(guild: Guild, role: Role): VoiceChannel {
        val tmpChannel = guild.createVoiceChannel("-" + role.name.substring(0, role.name.length - HardCodedValues.playerRoleSuffix.length), guild.getCategoryById(HardCodedValues.CATEGORY_GAMES_VOICE_CHATS)).complete()
        DeleteTempChannelOndisconnectBotOnDisconnect.onTmpGuildVoiceCreated(tmpChannel)
        return tmpChannel
    }
    
    @Suppress("MemberVisibilityCanBePrivate")
    fun trigger(guild: Guild, inviteAuthor: Member, role: Role, msg: String?): Message {
        val gameSpecificTxtChannel = ChannelUtils.getChannelsHavingRoleOverride(
            arrayOf(guild),
            arrayOf(role.idLong),
            arrayOf((ConfigHandler.config as CommonConfig).viewPermissionBit),
            arrayOf(HardCodedValues.CATEGORY_GAMES_TEXT_CHATS),
            text = true,
            voice = false
        ).findAny()
        
        val channel: MessageChannel =
            if (gameSpecificTxtChannel.isPresent) gameSpecificTxtChannel.get() as TextChannel
            else gateway.getTextChannelById(HardCodedValues.CHANNEL__GENERAL)!!
        
        channel.sendMessage(role.asMention).queue()
        val message = channel.sendMessageEmbeds(getEmbed(guild, inviteAuthor, role, msg)).complete()
        message.addReaction(Constants.EMOTE_SUCCESS.tounicode()).queue()
        
        addReactionEvent(message, Constants.EMOTE_SUCCESS, null) { reactMember, _, _ ->
            val answer = if (inviteAuthor.idLong != reactMember.idLong) CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_INVITE__ON_JOIN_MSG, inviteAuthor.asMention, reactMember.asMention)
            else CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_RETURN__GAME_INVITE__ON_JOIN_TROLL_MSG, reactMember.asMention)
            message.reply(answer).queue()
        }
        
        
        
        return message
    }
}