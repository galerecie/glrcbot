package com.bancarelvalentin.glrcbot.common.process.common.command.gamesupport

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import java.util.function.BiConsumer

class GameSupportRemoveTextCommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_SUPPORT_REMOVE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_SUPPORT_REMOVE__DESC)
    override val sample = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__GAME_SUPPORT_REMOVE__SAMPLE)
    
    override val whitelistRoleIds = arrayOf(HardCodedValues.ROLE__MAITRE_EQUIPAGE)
    override val patterns = arrayOf("game_support_remove", "gsr")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(RoleParam::class.java)
    
    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        val role = request.getParamRole(0)!!
        
        // Delete correct channels
        ChannelUtils.getChannelsHavingRoleOverride(
            arrayOf(request.guild!!),
            arrayOf(role.idLong),
            arrayOf((ConfigHandler.config as CommonConfig).viewPermissionBit),
            text = true,
            voice = true
        ).map { ch -> gateway.getTextChannelById(ch.id)!! }
            .forEach {
                if (it.parentCategory?.idLong == HardCodedValues.CATEGORY_GAMES_TEXT_CHATS)
                    it.manager
                        .setParent(gateway.getCategoryById(HardCodedValues.CATEGORY_ARCHIVES))
                        .sync()
                        .queue()
                else
                    it.delete().queue()
            }
        
        // Delete role
        role.delete().queue()
        
    }
}