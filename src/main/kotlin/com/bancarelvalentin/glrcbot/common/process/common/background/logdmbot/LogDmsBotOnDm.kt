package com.bancarelvalentin.glrcbot.common.process.common.background.logdmbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.PrivateChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.GenericMessageEvent

class LogDmsBotOnDm(process: Process) : EventListener(process) {
    
    override fun onGenericMessage(event: GenericMessageEvent) {
        super.onGenericMessage(event)
        if (event.channel is PrivateChannel) {
            event.channel.retrieveMessageById(event.messageId).queue { message ->
                if (message.embeds.isEmpty()) {
                    gateway.openPrivateChannelById(HardCodedValues.USER_VBA).queue { channel ->
                        channel.sendMessageEmbeds(toEmbed(event, (event.channel as PrivateChannel).user, message)).queue()
                    }
                }
            }
        }
    }
    
    private fun toEmbed(messageEvent: GenericMessageEvent, user: User, message: Message): MessageEmbed {
        return EmbedBuilder()
            .setTitle(messageEvent.javaClass.simpleName)
            .addField("Dm with", userToStr(user), false)
            .addField("From", userToStr(message.author), false)
            .setDescription(message.contentRaw)
            .build()
    }
    
    private fun userToStr(user: User) = user.asMention + " (" + user.name + ")"
}

