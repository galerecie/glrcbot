package com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.Constants
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent


class GameGrantsOnReactionAddEvent(process: Process) : EventListener(process) {
    
    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        if (event.user != null && !event.user!!.isBot && event.channel.id == GameGrantsHelper.assignationChannel.id) {
            val emoji = event.reactionEmote.emoji
            if (emoji == Constants.EMOTE_SUCCESS.tounicode() || emoji == Constants.EMOTE_ERROR.tounicode()) {
                GameGrantsHelper.assignationChannel.retrieveMessageById(event.messageId)
                    .queue {
                        val role = it.mentionedRoles[0]
                        event.guild.retrieveMember(event.user!!).queue { member ->
                            if (emoji == Constants.EMOTE_SUCCESS.tounicode()) {
                                event.guild.addRoleToMember(member, role).queue()
                            } else if (emoji == Constants.EMOTE_ERROR.tounicode()) {
                                event.guild.removeRoleFromMember(member, role).queue()
                            }
                        }
                    }
            }
        }
    }
}
