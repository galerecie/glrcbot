package com.bancarelvalentin.glrcbot.common.process.common.background.mentionreplybot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum


class MentionReplyBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__MENTION_REPLY__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__MENTION_REPLY__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(MentionReplyBotOnMessage(this))
    }
}