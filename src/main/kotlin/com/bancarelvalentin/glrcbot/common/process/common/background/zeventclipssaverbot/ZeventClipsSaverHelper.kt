package com.bancarelvalentin.glrcbot.common.process.common.background.zeventclipssaverbot

import com.bancarelvalentin.glrcbot.common.HardCodedValues
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Message
import java.util.regex.Matcher
import java.util.regex.Pattern

class ZeventClipsSaverHelper {
    
    companion object {
        
        //https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
        private val pattern: Pattern =
            Pattern.compile(".*(https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)).*")
        
        private var gateway: JDA? = null
        
        private var sent: ArrayList<String>? = null
        
        
        fun tryTo(gateway: JDA, message: Message) {
            init(gateway)
            
            try {// avoid gifs
                if (message.embeds[0].siteProvider!!.name == "Tenor") return
            } catch (e: Exception) {
            
            }
            if (message.reactions.isNotEmpty()) {
                val matcher: Matcher = pattern.matcher(message.contentRaw)
                if (matcher.matches()) {
                    val url = matcher.group(1)
                    if (url != null && !sent!!.contains(url)) {
                        sent!!.add(url)
                        gateway.getTextChannelById(HardCodedValues.CHANNEL__ZEVENTS_CLIPZ)!!
                            .sendMessage(url).queue()
                    }
                }
            }
        }
        
        fun tryAll(gateway: JDA) {
            sent = null
            init(gateway)
            gateway.getCategoryById(HardCodedValues.CATEGORY_EVENTS)!!.textChannels.filter { it.idLong != HardCodedValues.CHANNEL__ZEVENTS_CLIPZ }
                .forEach {
                    it.iterableHistory.queue { messages ->
                        val iterator = messages.iterator()
                        while (iterator.hasNext()) {
                            val next = iterator.next()
                            tryTo(gateway, next)
                        }
                    }
                }
        }
        
        private fun init(gateway: JDA) {
            if (sent == null) {
                Companion.gateway = gateway
                sent = ArrayList()
                val retrievePast = gateway.getTextChannelById(HardCodedValues.CHANNEL__ZEVENTS_CLIPZ)!!.iterableHistory
                retrievePast.queue {
                    val iterator = it.iterator()
                    while (iterator.hasNext()) {
                        val next = iterator.next()
                        if (sent!!.contains(next.contentRaw))
                            next.delete().queue()
                        else
                            sent!!.add(next.contentRaw)
                    }
                }
            }
        }
    }
}