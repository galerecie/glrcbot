package com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum


class GameGrantsBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__GAME_GRANTS__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__GAME_GRANTS__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(
            GameGrantsOnReactionAddEvent(this),
            GameGrantsOnReadyEvent(this),
            GameGrantsOnRoleCreateEvent(this),
            GameGrantsOnRoleUpdate(this),
            GameGrantsOnRoleDelete(this)
        )
    }
}