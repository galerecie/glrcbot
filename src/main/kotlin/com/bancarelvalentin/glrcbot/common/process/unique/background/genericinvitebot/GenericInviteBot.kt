package com.bancarelvalentin.glrcbot.common.process.unique.background.genericinvitebot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class GenericInviteBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__GENERIC_INVITE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__GENERIC_INVITE__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(GenericInviteBotOnGuildReady(this))
    }
}