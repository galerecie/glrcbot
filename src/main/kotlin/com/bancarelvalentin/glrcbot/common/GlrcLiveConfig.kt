package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.config.LiveConfig
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.Date


@Suppress("unused")
class GlrcLiveConfig : LiveConfig {
    
    @JsonProperty("birthdays")
    var birthdays: List<Birthday> = emptyList()
    
    class Birthday(
        @JsonProperty("id")
        var id: Long,
        @JsonProperty("name")
        var name: String,
        @JsonProperty("date")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        var date: Date
    )
}