package com.bancarelvalentin.glrcbot.exception

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class WrongBirthdayDateFormatException(dateStr: String) : AbstractUserException(Localizator.get(GlrcSimpleLocalizeEnum.COMMAND_DOC_ERROR__ADD_BIRTHDAY__WRONG_DATE_FORMAT, dateStr)) {

}
