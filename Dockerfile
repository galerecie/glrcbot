FROM openjdk:14
WORKDIR /
ADD ./build/libs/Bot.jar Bot.jar
CMD java -jar Bot.jar